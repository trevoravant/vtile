# vtile - Gnome Vertical Tiler

![Alt Text](animation.gif)

## Overview

This is a simple program that does the following to the active window:
* maximizes it vertically
* resizes it to a specified width
* positions it at a specified x-position

This program only works using Gnome and GTK, and is written using [libwnck](https://gitlab.gnome.org/GNOME/libwnck).
This program is written as a single C file which should be compiled to produce an executable.

## Requirements

Using Arch Linux, you will have to install the [libwnck3](https://archlinux.org/packages/extra/x86_64/libwnck3/) package to include the `libwnck` library in the C code.
Using a different Linux distribution, you will probably have to install a similar package.

## Compilation

Use the following command to compile the C file:
```bash
$ gcc -o vtile vtile.c -DWNCK_I_KNOW_THIS_IS_UNSTABLE `pkg-config --libs libwnck-3.0` `pkg-config --cflags libwnck-3.0`
```
This command is based on similar examples online, but I can't find the official documentation for the proper compilation command.

## Usage

### Syntax

The `vtile` command takes two inputs:
* desired x-position of the window, as a percent of the total screen width
* desired width of the window, as a percent of the total screen width

For example, the following command will put the window at an x-position that is 60% of the total screen width, and resize the window so its width is 20% of the total screen width:
```bash
$ ./vtile 60 20
```

### Keyboard Shortcuts

This command is most useful when assigned to a keyboard shortcut. To assign it to a shortcut in Gnome, go to Settings > Keyboard > Keyboard Shortcuts > View and Customize Shortcuts > Custom Shortcuts.
For example, I set <kbd>Super</kbd>+<kbd>1</kbd> to `/home/trevor/vtile/vtile 0 20`, I set <kbd>Super</kbd>+<kbd>2</kbd> to `/home/trevor/vtile/vtile 20 20`, and so on.
