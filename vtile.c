#include <libwnck/libwnck.h>

int main(int argc, char **argv)
{
	// wnck stuff
	WnckHandle *wnckhandle;
	WnckScreen *screen;
	WnckWindow *active_window;
	gdk_init(&argc, &argv);
	wnckhandle = wnck_handle_new(WNCK_CLIENT_TYPE_APPLICATION);
	screen = wnck_handle_get_default_screen(wnckhandle);
	wnck_screen_force_update(screen);
	active_window = wnck_screen_get_active_window(screen);

	// get screen width and height
	int w = wnck_screen_get_width(screen);
	int h = wnck_screen_get_height(screen);

	// get inputs and convert to pixel values
	int x_pct = atoi(argv[1]);
	int w_pct = atoi(argv[2]);
	int x_pix = x_pct*w/100;
	int w_pix = w_pct*w/100;

	// check if window is maximized fully, vertically, or horizontally
	// if so, we must unmaximize it before resizing it
	if (wnck_window_is_maximized(active_window)) {
		wnck_window_unmaximize(active_window);
	}
	else if (wnck_window_is_maximized_vertically(active_window)) {
		wnck_window_unmaximize_vertically(active_window);
	}
	else if (wnck_window_is_maximized_horizontally(active_window)) {
		wnck_window_unmaximize_horizontally(active_window);
	}

	// resize the window
	wnck_window_set_geometry(active_window,
				 WNCK_WINDOW_GRAVITY_NORTHWEST, 
				 WNCK_WINDOW_CHANGE_X |
				 WNCK_WINDOW_CHANGE_Y |
				 WNCK_WINDOW_CHANGE_WIDTH |
				 WNCK_WINDOW_CHANGE_HEIGHT,
				 x_pix, 0, w_pix, h);

	return 0;
}
